package com.egip.interactivemap.mapper;

import com.egip.interactivemap.dto.CoordinateDTO;
import com.egip.interactivemap.entity.Coordinate;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CoordinateMapper extends AbstractMapper<Coordinate, CoordinateDTO> {
}
