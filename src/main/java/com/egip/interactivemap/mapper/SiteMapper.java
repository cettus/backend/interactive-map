package com.egip.interactivemap.mapper;

import com.egip.interactivemap.dto.SiteDTO;
import com.egip.interactivemap.entity.Site;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SiteMapper extends AbstractMapper<Site, SiteDTO> {
}
