package com.egip.interactivemap.mapper;

import com.egip.interactivemap.dto.GeometryDTO;
import com.egip.interactivemap.entity.Geometry;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GeometryMapper extends AbstractMapper<Geometry, GeometryDTO> {
}
