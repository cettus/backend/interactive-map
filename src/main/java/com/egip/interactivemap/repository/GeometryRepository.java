package com.egip.interactivemap.repository;

import com.egip.interactivemap.entity.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GeometryRepository extends JpaRepository<Geometry, Long> {
}
