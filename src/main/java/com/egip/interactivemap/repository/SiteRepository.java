package com.egip.interactivemap.repository;

import com.egip.interactivemap.entity.Site;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SiteRepository extends JpaRepository<Site, Long>, JpaSpecificationExecutor<Site> {
    @Query("select s from Site as s " +
            "inner join s.geometry as g " +
            "inner join g.coordinates as c " +
            "where c.x > :xMin and c.x < :xMax " +
            "and c.y > :yMin and c.y < :yMax")
    Page<Site> search(Double xMin, Double xMax, Double yMin, Double yMax, Pageable pageable);

    List<Site> findByName(String name);
}
