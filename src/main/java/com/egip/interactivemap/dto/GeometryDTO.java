package com.egip.interactivemap.dto;

import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.json.JsonParseException;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class GeometryDTO {
    private String type;
    private List<CoordinateDTO> coordinates;

    public GeometryDTO() {
        this.coordinates = new ArrayList<>();
    }

    @JsonSetter(value = "coordinates")
    public void setJsonCoordinates(List<List<List<Double>>> rawCoordinates) {
        if (rawCoordinates.size() > 1) {
            throw new JsonParseException();
        }
        List<List<Double>> firstLayerExtractedList = rawCoordinates.get(0);
        for (List<Double> secondLayerExtractedList : firstLayerExtractedList) {
            this.coordinates.add(new CoordinateDTO(secondLayerExtractedList.get(0), secondLayerExtractedList.get(1)));
        }
    }

    @Override
    public String toString() {
        return "GeometryDTO{" +
                "type='" + type + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }
}
