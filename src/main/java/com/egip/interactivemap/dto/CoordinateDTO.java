package com.egip.interactivemap.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CoordinateDTO {
    private Double x;
    private Double y;

    public CoordinateDTO(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public CoordinateDTO() {
    }

    @Override
    public String toString() {
        return "CoordinateDTO{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
