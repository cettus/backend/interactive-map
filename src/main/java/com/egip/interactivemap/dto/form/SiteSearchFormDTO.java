package com.egip.interactivemap.dto.form;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SiteSearchFormDTO {
    private Double x;
    private Double y;
    private Double xMaxDistance;
    private Double yMaxDistance;
    private String name;
}
