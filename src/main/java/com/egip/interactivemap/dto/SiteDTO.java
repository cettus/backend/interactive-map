package com.egip.interactivemap.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SiteDTO {
    private Long id;
    private String name;
    private GeometryDTO geometry;
    private String initialBorder;
    private String endBorder;
    private String district;
    private String reason;
    private String programRepair;
    private String categoryObject;
    private Double areaCarriageway;
    private Double areaSidewalk;
    private Double areaCurb;
    private Double areaSum;


    @Override
    public String toString() {
        return "ObjectDTO{" +
                "name='" + name + '\'' +
                ", initialBorder='" + initialBorder + '\'' +
                ", endBorder='" + endBorder + '\'' +
                ", district='" + district + '\'' +
                ", reason='" + reason + '\'' +
                ", programRepair='" + programRepair + '\'' +
                ", categoryObject='" + categoryObject + '\'' +
                ", areaCarriageway='" + areaCarriageway + '\'' +
                ", areaSidewalk='" + areaSidewalk + '\'' +
                ", areaCurb='" + areaCurb + '\'' +
                ", areaSum='" + areaSum + '\'' +
                ", geometryDTO=" + geometry +
                '}';
    }
}
