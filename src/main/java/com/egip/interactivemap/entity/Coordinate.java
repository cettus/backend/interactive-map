package com.egip.interactivemap.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
public class Coordinate {
    private Double x;
    private Double y;
}
