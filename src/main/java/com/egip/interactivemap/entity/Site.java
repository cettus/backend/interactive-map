package com.egip.interactivemap.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Site {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @OneToOne
    private Geometry geometry;

    @Column
    private String initialBorder;

    @Column
    private String endBorder;

    @Column
    private String district;

    @Column
    private String reason;

    @Column
    private String programRepair;

    @Column
    private String categoryObject;

    @Column
    private Double areaCarriageway;

    @Column
    private Double areaSidewalk;

    @Column
    private Double areaCurb;

    @Column
    private Double areaSum;
}
