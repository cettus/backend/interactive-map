package com.egip.interactivemap.service;

import com.egip.interactivemap.dto.SiteDTO;
import com.egip.interactivemap.dto.form.SiteSearchFormDTO;
import com.egip.interactivemap.entity.Site;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface SiteService {
    List<SiteDTO> parseJson(MultipartFile file);
    void parseAndSave(MultipartFile file);
    Page<Site> read(SiteSearchFormDTO siteSearchFormDTO);
    List<SiteDTO> parseExcelFile(MultipartFile file);
    void parseAndSaveExcel(MultipartFile file);
}
