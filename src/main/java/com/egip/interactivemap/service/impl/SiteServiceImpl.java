package com.egip.interactivemap.service.impl;

import com.egip.interactivemap.dto.SiteDTO;
import com.egip.interactivemap.dto.form.SiteSearchFormDTO;
import com.egip.interactivemap.entity.Coordinate;
import com.egip.interactivemap.entity.Geometry;
import com.egip.interactivemap.entity.Site;
import com.egip.interactivemap.mapper.CoordinateMapper;
import com.egip.interactivemap.repository.GeometryRepository;
import com.egip.interactivemap.repository.SiteRepository;
import com.egip.interactivemap.service.SiteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class SiteServiceImpl implements SiteService {
    private final ObjectMapper objectMapper;
    private final SiteRepository siteRepository;
    private final GeometryRepository geometryRepository;
    private final CoordinateMapper coordinateMapper;
    private final EntityManager entityManager;

    @Autowired
    public SiteServiceImpl(ObjectMapper objectMapper,
                           SiteRepository siteRepository,
                           GeometryRepository geometryRepository,
                           CoordinateMapper coordinateMapper,
                           EntityManager entityManager) {
        this.objectMapper = objectMapper;
        this.siteRepository = siteRepository;
        this.geometryRepository = geometryRepository;
        this.coordinateMapper = coordinateMapper;
        this.entityManager = entityManager;
    }

    @Override
    public List<SiteDTO> parseJson(MultipartFile file) {
        List<SiteDTO> sites = new ArrayList<>();
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            //ignore
        }
        new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .forEach(
                        s -> {
                            try {
                                SiteDTO siteDTO = objectMapper.readValue(s, SiteDTO.class);
                                sites.add(siteDTO);
                            } catch (IOException e) {
                                //ignore
                            }
                        }
                );
        return sites;
    }

    @Override
    public void parseAndSave(MultipartFile file) {
        List<SiteDTO> sites = parseJson(file);
        for (SiteDTO siteDTO : sites) {
            Site site = new Site();
            site.setName(siteDTO.getName());
            Geometry geometry = new Geometry();
            geometry.setType(siteDTO.getGeometry().getType());
            List<Coordinate> coordinates = coordinateMapper.toListEntities(siteDTO.getGeometry().getCoordinates());
            geometry.setCoordinates(coordinates);
            geometry = geometryRepository.save(geometry);
            site.setGeometry(geometry);
            siteRepository.save(site);
        }
    }

    @Override
    @Transactional
    public Page<Site> read(SiteSearchFormDTO formDTO) {
        return siteRepository.search(
                formDTO.getX() - formDTO.getXMaxDistance(),
                formDTO.getX() + formDTO.getXMaxDistance(),
                formDTO.getY() - formDTO.getYMaxDistance(),
                formDTO.getY() + formDTO.getYMaxDistance(),
                Pageable.unpaged()
        );
    }

    @Override
    public List<SiteDTO> parseExcelFile(MultipartFile file) {


        List<SiteDTO> sites = new ArrayList<>();
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();

            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (row.getRowNum() < 10) continue;

                String name = row.getCell(1).getStringCellValue();
                if (siteRepository.findByName(name).size() > 0) {

                    SiteDTO siteDTO = new SiteDTO();

                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        switch (cell.getColumnIndex()) {
                            case 1:
                                siteDTO.setName(cell.getStringCellValue());
                                break;
                            case 2:
                                siteDTO.setInitialBorder(cell.getStringCellValue());
                                break;
                            case 3:
                                siteDTO.setEndBorder(cell.getStringCellValue());
                                break;
                            case 4:
                                siteDTO.setDistrict(cell.getStringCellValue());
                                break;
                            case 5:
                                siteDTO.setReason(cell.getStringCellValue());
                                break;
                            case 6:
                                siteDTO.setProgramRepair(cell.getStringCellValue());
                                break;
                            case 7:
                                siteDTO.setCategoryObject(cell.getStringCellValue());
                                break;
                            case 8:
                                siteDTO.setAreaCarriageway(cell.getNumericCellValue());
                                break;
                            case 9:
                                siteDTO.setAreaSidewalk(cell.getNumericCellValue());
                                break;
                            case 10:
                                siteDTO.setAreaCurb(cell.getNumericCellValue());
                                break;
                            case 11:
                                siteDTO.setAreaSum(cell.getNumericCellValue());
                                break;
                            default:
                                break;
                        }

                    }
                    sites.add(siteDTO);
                }

            }

        } catch (IOException e) {
            //ignore
        }

        return sites;

    }

    @Override
    public void parseAndSaveExcel(MultipartFile file) {
        List<SiteDTO> sites = parseExcelFile(file);
        for (SiteDTO site : sites) {
            siteRepository.findByName(site.getName()).forEach(s ->
                    {
                        s.setInitialBorder(site.getInitialBorder());
                        s.setEndBorder(site.getEndBorder());
                        s.setDistrict(site.getDistrict());
                        s.setReason(site.getReason());
                        s.setProgramRepair(site.getProgramRepair());
                        s.setCategoryObject(site.getCategoryObject());
                        s.setAreaCarriageway(site.getAreaCarriageway());
                        s.setAreaSidewalk(site.getAreaSidewalk());
                        s.setAreaCurb(site.getAreaCurb());
                        s.setAreaSum(site.getAreaSum());
                        siteRepository.save(s);
                    }
                    );
        }
    }
}
