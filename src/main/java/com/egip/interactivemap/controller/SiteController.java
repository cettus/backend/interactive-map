package com.egip.interactivemap.controller;

import com.egip.interactivemap.dto.SiteDTO;
import com.egip.interactivemap.dto.form.SiteSearchFormDTO;
import com.egip.interactivemap.entity.Site;
import com.egip.interactivemap.mapper.SiteMapper;
import com.egip.interactivemap.service.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping(path = "/site")
public class SiteController {
    private final SiteService siteService;
    private final SiteMapper siteMapper;

    @Autowired
    public SiteController(SiteService siteService,
                          SiteMapper siteMapper) {
        this.siteService = siteService;
        this.siteMapper = siteMapper;
    }

    @PostMapping(path = "/parse", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void parse(@RequestParam("file") MultipartFile file) {
        siteService.parseAndSave(file);
    }

    @GetMapping(path = "/read")
    public Page<SiteDTO> read(SiteSearchFormDTO form) {
        Page<Site> sites = siteService.read(form);
        return new PageImpl<>(siteMapper.toListDTOs(sites.getContent()));
    }

    @PostMapping(path = "/parse-excel", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void parseExcel(@RequestParam("file") MultipartFile file) {
        siteService.parseAndSaveExcel(file);
    }
}
